import librosa
import wave
from scipy.io import wavfile as wav
from sklearn.externals import joblib

from sklearn.ensemble import RandomForestClassifier
from sklearn.decomposition import PCA

f = '/Users/cim/Documents/Ariot/BabySounds/0f257dac-7d6f-4575-9192-e3b4dcd3d4ef-1430185476161-1.7-f-26-lo.3gp.wav'
rate, wav_sample = wav.read(f)
wav_sample = wav_sample[:8000*5]
wav_sample = wav_sample.astype(float)
S = librosa.feature.melspectrogram(y=wav_sample, sr=8000)
tmp = S.ravel()
tmp = tmp/max(abs(tmp))
print(tmp.shape)
tmp = [tmp]
model_clone = joblib.load('my_model.pkl')
model_clone_pca = joblib.load('my_model_pca.pkl')

tmp = model_clone_pca.transform(tmp)
result = model_clone.predict(tmp)

print (type(result[0]))
result = list(result[0])
if result == [1,0,0,0,0,0,0,0,0]:
    r = 'hungry'
elif result == [0,1,0,0,0,0,0,0,0]:
    r = 'needs burping'
elif result == [0,0,1,0,0,0,0,0,0]:
    r = 'belly pain'
elif result == [0,0,0,1,0,0,0,0,0]:
    r = 'discomfort'
elif result == [0,0,0,0,1,0,0,0,0] :
    r =  'tired'      
elif result == [0,0,0,0,0,1,0,0,0]:
    r = 'lonely'
elif result == [0,0,0,0,0,0,1,0,0]:
    r = 'cold/hot'
elif result == [0,0,0,0,0,0,0,1,0]:
    r = 'scared'
elif result == [0,0,0,0,0,0,0,0,1]:
    r =   'don\'t know'
else:
    r = 'no worries'
